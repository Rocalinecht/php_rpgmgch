<?php

class User
{
    private $_id;
    private $_email;
    private $_date;

    function __construct($i, $m, $d) {
        $this->id = $i;
        $this->email = $m;
        $this->date = $d.date("d") . "-" . date("m") . "-" . date("Y");
    }
  
    function getId(){
       return  $this->id;
    }
    function getEmail(){
        return $this->email;
    }
    function createDate(){
        return $this->date;
    }


}

