<?php

require "User.php";
require "Client.php";


$client1 = new Client(1, "premierclient@gmail.com",$_date);
$client2 = new Client(2, "deuxièmeclient@gmail.com",$_date);

return [
    $client1,
    $client2
];

