<?php

require "Product.php";
require "Vegetable.php";
require "Cloth.php";

$legume1 = new Vegetable(1,"courgettes","1€","legume",$_actualDate,$_expiresAt);
$legume2 = new Vegetable(2,"tomates","2€","fruit",$_actualDate,$_expiresAt);
$cloth1= new Cloth(3,"chemise","85€","Lacoste");
$cloth2= new Cloth(4,"pantalon","40€","Levis");
$cloth3= new Cloth(5,"t-shirt","25€","Nike");


return [
    $legume1,
    $legume2,
    $cloth1,
    $cloth2,
    $cloth3
];