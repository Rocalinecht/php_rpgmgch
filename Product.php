<?php
class Product{
    private $_id;
    private $_name;
    private $_price;
    

    function __construct($i, $n, $p) {
        $this->id = $i;
        $this->name = $n;
        $this->price = $p;
        
    }
    public function getId(){
        return $this->id;
    }
    public function getName(){
        return $this->name;
    }
    public function getPrice(){
        return $this->price;
    }

}